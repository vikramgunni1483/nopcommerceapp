from selenium import webdriver
import pytest

@pytest.fixture()
def setup(browser):
    if browser=='chrome':
        driver = webdriver.Chrome(executable_path="C:\\Users\\vikram\\Downloads\\chromedriver_win32\\chromedriver.exe")
        print("Launching Chrome browser")
    elif browser=='firefox':
        driver=webdriver.Firefox(executable_path="C:\\Users\\vikram\\Downloads\\geckodriver-v0.27.0-win64\\geckodriver.exe")
        print("Launching Firefox browser")

    return driver              # Returning the driver next

def pytest_addoption(parser):  # This will get the value from CLI/hooks
    parser.addoption("--browser")

@pytest.fixture()
def browser(request):  # This will return the browser value to the setup method
    return request.config.getoption("--browser")


########## Pytest HTML Report ##################

# It is a hook for adding environment info into HTML Report
def pytest_configure(config):
    config._metadata['Project Name']='nop Commerce'
    config._metadata['Module Name']='Customers'
    config._metadata['Tester']='Vikram'

# It is a hook for deleting/modifying environment info into HTML Report
@pytest.mark.optionalhook
def pytest_metadata(metadata):
    metadata.pop("JAVA_HOME",None)
    metadata.pop("Plugins",None)



# Previously it was like this
# @pytest.fixture()
# def setup():
#     driver=webdriver.Chrome(executable_path="C:\\Users\\vikram\\Downloads\\chromedriver_win32\\chromedriver.exe")
#     return driver