import pytest
from selenium import webdriver
# from packageName.ModuleName import ClassName
from pageObjects.LoginPage import LoginPage
from utilities.readProperties import ReadConfig
from utilities.customLogger import LogGen
from utilities import XLUtils
import time

class Test_002_DDT_Login:
    baseURL=ReadConfig.getApplicationURL()
    path=".//TestData/LoginData.xlsx"

    logger=LogGen.loggen()

    @pytest.mark.regression
    def test_login_DDT(self,setup):
        self.logger.info("************** Test_002_DDT_Login **********")
        self.logger.info("************** Verifying Login DDT test **********")

        self.driver=setup
        self.driver.get(self.baseURL)

        # Calling the action methods of the LoginPage Class
        # through its created objects.
        # All are related to the class so we have to use "self"
        # LoginPage class has a constructor which expects a
        # driver as an argument so we have to pass the driver as arg accordingly

        self.lp=LoginPage(self.driver)

        self.rows=XLUtils.getRowCount(self.path,'Sheet1')
        print("Number of rows in the Excel are ",self.rows)

        lst_status=[]  # Empty List Variable

        for r in range(2,self.rows+1):
            self.user=XLUtils.readData(self.path,'Sheet1',r,1)
            self.password=XLUtils.readData(self.path,'Sheet1', r, 2)
            self.exp=XLUtils.readData(self.path, 'Sheet1', r, 3)


            self.lp.setUserName(self.user)
            self.lp.setPassword(self.password)
            self.lp.clickLogin()
            time.sleep(5)

            actual_title=self.driver.title
            exp_title="Dashboard / nopCommerce administration"


            if actual_title==exp_title:
                if self.exp=="Pass":
                    self.logger.info("***** Passed ****")
                    self.lp.clickLogout()   # To logout and re-login using next set of credentials
                    lst_status.append("Pass")

                elif self.exp=="Fail":
                    self.logger.error("***** Failed ****")
                    self.lp.clickLogout()  # To logout and re-login using next set of credentials
                    lst_status.append("Fail")


            elif actual_title!=exp_title:
                if self.exp=="Pass":
                    self.logger.error("***** Failed ****")
                    lst_status.append("Fail")

                elif self.exp=="Fail":
                    self.logger.info("**** Passed ****")
                    lst_status.append("Pass")


        if "Fail" not in lst_status:
                self.logger.info("**** Login DDT test passed ****")
                self.driver.close()
                assert True

        else:
                self.logger.error("**** Login DDT test failed ****")
                self.driver.close()
                assert False

        self.logger.info("****** End Of Login DDT test *********")
        self.logger.info("****** Completed TC_LoginDDT_002 **********")

