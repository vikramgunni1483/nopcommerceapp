import pytest
from selenium import webdriver
# from packageName.ModuleName import ClassName
from pageObjects.LoginPage import LoginPage
from utilities.readProperties import ReadConfig
from utilities.customLogger import LogGen

class Test_001_Login:
    baseURL=ReadConfig.getApplicationURL()
    username=ReadConfig.getUseremail()
    password=ReadConfig.getPassword()

    logger=LogGen.loggen()

    @pytest.mark.regression
    def test_homePageTitle(self,setup):
        self.logger.info("**************** Test_001_Login *************")
        self.logger.info("**************** Verifying Home Page Title ***********")

        self.driver=setup    # Here the fixture method setup() in the argument  will return the driver
        self.driver.get(self.baseURL)
        actual_title=self.driver.title


        if actual_title=="Your store. Login":
            assert True
            self.driver.close()
            self.logger.info("************ Home Page Title Test Got Passed")

        else:
            # Here (.) represents the current project
            # And (\\) represents within that folder
            self.driver.save_screenshot(".\\Screenshots\\"+"test_homePageTitle.png")
            self.driver.close()
            self.logger.error("************ Home Page Title Test Failed ")
            assert False


    @pytest.mark.sanity
    @pytest.mark.regression
    def test_login(self,setup):
        self.logger.info("************* Verifying The Login Test ********")

        self.driver=setup
        self.driver.get(self.baseURL)

        # Calling the action methods of the LoginPage Class
        # through its created objects.
        # All are related to the class so we have to use "self"
        # LoginPage class has a constructor which expects a
        # driver as an argument so we have to pass the driver as arg accordingly

        self.lp=LoginPage(self.driver)
        self.lp.setUserName(self.username)
        self.lp.setPassword(self.password)
        self.lp.clickLogin()

        actual_title=self.driver.title
        self.driver.close()

        if actual_title=="Dashboard / nopCommerce administration":
            assert True
            self.logger.info("********* Login Test Got Passed *********")
            self.driver.quit()
        else:
            self.driver.save_screenshot(".\\Screenshots\\"+"test_login.png")
            self.driver.quit()
            self.logger.error("********* Login Test Failed *********")
            assert False






